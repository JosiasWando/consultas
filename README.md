# Backend Relatório e Dashboard #

Este é o backend MPV(Mínimo produto viável) de um site de consultas de dados médicos. Se trata de uma api rest para obter as informações. Você pode acessar a api por esta url:

https://consulta-backend.herokuapp.com/api/

## Instruções de instalação 

### Pipenv

Este projeto utiliza [pipenv](https://pipenv-fork.readthedocs.io/en/latest/) como gerenciador de pacotes. É um gerenciador moderno e que corrige certos problemas no pip tradicional, como por exemplo, ser capaz de realizar builds deterministicos.

Para instalar este software, é necessário instalar o pipenv.

#### ubuntu

```bash
pip install pipenv
```
**Atenção** A não ser que configurado previamente, o ubuntu ainda utiliza por padrão o python 2. o pipenv só é compativel com o python 3. Logo, talvez  você precise executar esse comando:

```bash
pip3 install pipenv
```
talvez seja necessário reiniciar o pc. Quanto terminar, veja se foi instalado corretamente:

```bash
pipenv --version 
```

#### fedora

Para instalar no fedora, use o comando:

```bash
dnf install pipenv 
```
 Quanto terminar, veja se foi instalado corretamente:

```bash
pipenv --version 
```
### Preparando o ambiente

Se estiver tudo okay, abra a pasta /backend em um terminal. Lá execute o seguinte comando:

```
pipenv install --dev --three
```

Isso instalará as dependencias principais do arquivo pipefile,(incluisve, é a referencia aonde o comando deve ser executado) e dizerá que você quer usar o python versão 3. Este comando também é usado para criar novos projetos. A flag '--dev' indica que tambḿe se deve instalar as dependencias de desenvolvimento, caso julgue necessário.

Feito o comando, uma virtualenv foi criada automáticamente. Para entrar dentro dela, use:

```
pipenv shell
```

Você estará dentro do ambiente.

### Subindo o backend

Na raiz do projeto, você precisa criar um arquivo .env com as seguintes informações:

```env
SECRET_KEY=(<Secret key do django>
DEBUG=<Se a aplicação estar em debug ou não.use true ou false. O default é false>
ALLOWED_HOSTS=<Lista de dominios e hosts que podem acessar a aplicação, separados por vírgula>
DB_HOST=<Coneção com o host do banco de dados>
DB_NAME=<Nome do banco de dados>
DB_USER=<Usuário do banco de dados>
DB_PORT=<Porta do banco de dados>
DB_PASSWORD=<senha do banco>
```

Estas variáveis de ambiente são disponibilizadas automáticamente pelo pipenv. Caso pretenda fazer deploy, é necessário coloca-las no ambiente.

Para subir o servidor de dev, entre na vitualenv e depois na pasta do backend

```
cd backend
```

Execute as migrations:

```
Python manage.py migrate
```

Por ser um MPV, não criei métodos para criar usuários com antecedencia. Isso não deve ser um problema visto que a principal função desta api é consulta.

Execute a API localmente com:

```python
python manage.py runserver
```

## Consultas
A url para consultas é https://consulta-backend.herokuapp.com/api. alguns de seus recursos:

### consultas
```url
/consultas/
```
É possível passar os seguintes filtros para ela:

* nome_medico:Retorna todos os resultados com aquele nome.

```url
https://consulta-backend.herokuapp.com/api/consultas/?nome_medico=luane
```

* periodo: retorna todas as consultas daquela data.
```url
https://consulta-backend.herokuapp.com/api/consultas/?periodo=2019-06-20
```

É possível consultar a base combinando ambos os filtros.

```url
https://consulta-backend.herokuapp.com/api/consultas/?periodo=2019-06-20&nome_medico=luane
```
### dashboard
```url
/consultas/dashboad/
```

esta especificação de consulta retorna os dados do dashboard. É obrigatório passar o seguinte parametro para o retorno:

```url
/consultas/dashboard/?competencia=2020-05
```

O formato é __yyyy-mm__ sempre nessa ordem. O resultado é algo como:

```json
{
    "consulta_media": 70,
    "exame_media": 2.41,
    "valor_consulta_media": 78.74
}
```

