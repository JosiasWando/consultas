from django_filters import rest_framework as filters
from consulta.models import Consulta

class ConsultarFilter(filters.FilterSet):
    nome_medico = filters.CharFilter(field_name="nome_medico", lookup_expr="iexact")
    periodo = filters.DateFilter(field_name="data_consulta")

    class Meta:
        model = Consulta
        fields = ['nome_medico', "periodo"]