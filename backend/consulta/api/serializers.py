from rest_framework import serializers
from consulta.models import Consulta


class ConsultaSerializer(serializers.ModelSerializer):
    # Estes campos são propreties
    qtde_exame = serializers.ReadOnlyField()
    total_exame = serializers.ReadOnlyField()
    valor_total = serializers.ReadOnlyField()

    class Meta:
        model = Consulta
        fields = ('numero_guia', 'nome_medico', 'data_consulta',
                  'valor_consulta', 'total_exame', 'valor_total', 'qtde_exame')

class DashboardSerializer(serializers.Serializer):
    consulta_media = serializers.IntegerField()
    exame_media = serializers.DecimalField(1000, 2, coerce_to_string=False)
    valor_consulta_media = serializers.DecimalField(1000, 2, coerce_to_string=False)
