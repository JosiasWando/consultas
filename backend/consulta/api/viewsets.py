from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from util.data import data_parans
from consulta.models import Dashboard
from .serializers import Consulta, ConsultaSerializer, DashboardSerializer
from .filters import ConsultarFilter


class ConsultaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Consulta.objects.all()
    serializer_class = ConsultaSerializer
    filterset_class = ConsultarFilter

    @action(detail=False, methods=['get'])
    def dashboard(self, request):
        try:
            competencia_values = data_parans(
                request.query_params['competencia'])
            dashboard = Dashboard(Consulta.consulta_average(competencia_values[0], competencia_values[1]), Consulta.exame_average(
                competencia_values[0], competencia_values[1]), Consulta.consulta_total_average(competencia_values[0], competencia_values[1]))
            return Response(data=DashboardSerializer(dashboard).data, status=status.HTTP_200_OK)
        except MultiValueDictKeyError as e:
            return Response(data='Não pode pedir por dashboard sem definir a competência', status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response(data=e.args[0], status=status.HTTP_400_BAD_REQUEST)
