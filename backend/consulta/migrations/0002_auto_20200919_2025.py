# Generated by Django 2.2 on 2020-09-19 20:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consulta', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consulta',
            name='valor_consulta',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=65535, null=True),
        ),
    ]
