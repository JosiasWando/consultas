from django.db import models
from datetime import datetime
import decimal

# Modelo de consulta vindo do Banco de Dados


class Consulta(models.Model):
    numero_guia = models.IntegerField(primary_key=True)
    cod_medico = models.IntegerField()
    nome_medico = models.CharField(max_length=50, blank=True, null=True)
    data_consulta = models.DateField(blank=True, null=True)
    valor_consulta = models.DecimalField(
        max_digits=1000, decimal_places=2, blank=True, null=True)

    @property
    def qtde_exame(self):
        return self.exame_set.count()

    @property
    def total_exame(self):
        total_exame = 0
        for exame in self.exame_set.all():
            total_exame += exame.valor_exame

        return total_exame

    @property
    def valor_total(self):
        return decimal.Decimal(self.valor_consulta) + self.total_exame

    @classmethod
    def _consultas_por_mes(self, competence_month, competence_year):
        return Consulta.objects.filter(data_consulta__month=competence_month, data_consulta__year=competence_year)

    @classmethod
    def consulta_average(self, competence_month, competence_year):
        return self._consultas_por_mes(competence_month, competence_year).count()

    @classmethod
    def exame_average(self, competence_month, competence_year):
        consultas = self._consultas_por_mes(competence_month, competence_year)
        if consultas.count() == 0:
            return 0
        exames = 0
        for consulta in consultas:
            exames += consulta.qtde_exame
        return exames/consultas.count()

    @classmethod
    def consulta_total_average(self, competence_month, competence_year):
        consultas = self._consultas_por_mes(competence_month, competence_year)
        if consultas.count() == 0:
            return 0
        total = 0
        for consulta in consultas:
            total += consulta.valor_total
        return total/consultas.count()

    class Meta:
        db_table = 'consulta'

class Dashboard:
    def __init__(self, consulta_media, exame_media, valor_consulta_media):
        self.consulta_media = consulta_media
        self.exame_media = exame_media
        self.valor_consulta_media = valor_consulta_media