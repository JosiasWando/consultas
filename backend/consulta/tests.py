import json
from django.test import TestCase
from django.urls import include, path, reverse, reverse_lazy
from rest_framework.test import APITestCase, URLPatternsTestCase
from .models import Consulta
from exame.models import Exame
from rest_framework import status
# Create your tests here.


class ConsultaModelTest(TestCase):
    def test_can_create_consulta(self):
        consulta = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)

        consulta_bd = Consulta.objects.get(numero_guia=1)
        self.assertIsNotNone(consulta_bd, "A consulta não foi registrada")
        self.assertEqual(consulta.numero_guia, consulta_bd.numero_guia)


class ConsultaPropetyTest(TestCase):
    def setUp(self):
        self.consulta_a = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)
        self.exame_a1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="RaioX", valor_exame=25.00)
        self.exame_a2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="Glicemia", valor_exame=10.00)

        self.consulta_b = Consulta.objects.create(
            numero_guia=2, cod_medico=2, nome_medico="Chopper", data_consulta="2020-05-02", valor_consulta=40.00)
        self.exame_b1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Ultrasom", valor_exame=25.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="HIV", valor_exame=10.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Biopsia", valor_exame=10.00)

    def test_can_count_exames_property(self):
        nun_exames_a = self.consulta_a.qtde_exame
        nun_exame_b = self.consulta_b.qtde_exame

        self.assertEqual(nun_exames_a, 2)
        self.assertEqual(nun_exame_b, 3)

    def test_can_sum_exames_property(self):
        exame_total_a = self.consulta_a.total_exame
        exame_total_b = self.consulta_b.total_exame

        self.assertEqual(exame_total_a, 35.00)
        self.assertEqual(exame_total_b, 45.00)

    def test_can_get_consulta_valor_total(self):
        consulta_total_a = self.consulta_a.valor_total
        consulta_total_b = self.consulta_b.valor_total

        self.assertEqual(consulta_total_a, 85.00)
        self.assertEqual(consulta_total_b, 85.00)


class ConsultaRestTest(APITestCase):

    def setUp(self):
        self.consulta_a = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)
        self.exame_a1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="RaioX", valor_exame=25.00)
        self.exame_a2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="Glicemia", valor_exame=10.00)

        self.consulta_b = Consulta.objects.create(
            numero_guia=2, cod_medico=2, nome_medico="Chopper", data_consulta="2020-05-02", valor_consulta=40.00)
        self.exame_b1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Ultrasom", valor_exame=25.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="HIV", valor_exame=10.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Biopsia", valor_exame=10.00)

    def test_can_get_consultas(self):
        url = '/api/consultas/'
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(data['results']), 2)

    def test_data_como_how_expect(self):
        test_data = [
            {
                'numero_guia': 1, 'nome_medico': 'Francisco', 'data_consulta': '2020-06-02',
                'valor_consulta': "50.00", 'total_exame': 35.00, 'valor_total': 85.00, 'qtde_exame': 2
            },
            {

                'numero_guia': 2, 'nome_medico': 'Chopper', 'data_consulta': '2020-05-02',
                'valor_consulta': "40.00", 'total_exame': 45, 'valor_total': 85, 'qtde_exame': 3
            }
        ]
        url = '/api/consultas/'
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(data['results'], test_data)


class ConsultarFilterTest(APITestCase):
    def setUp(self):
        self.consulta_a = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)
        self.exame_a1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="RaioX", valor_exame=25.00)

        self.consulta_b = Consulta.objects.create(
            numero_guia=2, cod_medico=2, nome_medico="Chopper", data_consulta="2020-05-02", valor_consulta=40.00)
        self.exame_b1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Ultrasom", valor_exame=25.00)

        self.consulta_c = Consulta.objects.create(
            numero_guia=3, cod_medico=3, nome_medico="Drauzio", data_consulta="2020-06-04", valor_consulta=50.00)
        self.exame_c1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_c, exame="Glicose", valor_exame=25.00)

        self.consulta_d = Consulta.objects.create(
            numero_guia=4, cod_medico=1, nome_medico="Francisco", data_consulta="2020-05-02", valor_consulta=50.00)
        self.exame_d1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_d, exame="Biopsia", valor_exame=25.00)

    def test_can_filter_by_name(self):
        url = '/api/consultas/?nome_medico=Francisco'
        response = self.client.get(url)

        self.assertEqual(len(response.data), 4)
        data = json.loads(response.content)

        self.assertEqual(data['results'][0]['nome_medico'], 'Francisco')
        self.assertEqual(data['results'][1]['nome_medico'], 'Francisco')

    def test_can_filter_by_date(self):
        url = '/api/consultas/?periodo=2020-05-02'
        response = self.client.get(url)

        self.assertEqual(len(response.data), 4)
        data = json.loads(response.content)

        self.assertEqual(data['results'][0]['nome_medico'], 'Chopper')
        self.assertEqual(data['results'][1]['nome_medico'], 'Francisco')

    def test_can_filter_by_date_and_medico(self):
        url = '/api/consultas/?periodo=2020-05-02&nome_medico=Francisco'
        response = self.client.get(url)

        self.assertEqual(len(response.data), 4)
        data = json.loads(response.content)

        self.assertEqual(data['results'][0]['nome_medico'], 'Francisco')


class ConsultaDashboardTest(TestCase):
    def setUp(self):
        self.consulta_a = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)
        self.exame_a1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="RaioX", valor_exame=25.00)

        self.consulta_b = Consulta.objects.create(
            numero_guia=2, cod_medico=2, nome_medico="Chopper", data_consulta="2020-05-02", valor_consulta=40.00)
        self.exame_b1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Ultrasom", valor_exame=25.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Punção", valor_exame=45.00)

        self.consulta_c = Consulta.objects.create(
            numero_guia=3, cod_medico=3, nome_medico="Drauzio", data_consulta="2020-06-04", valor_consulta=50.00)
        self.exame_c1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_c, exame="Glicose", valor_exame=25.00)

        self.consulta_d = Consulta.objects.create(
            numero_guia=4, cod_medico=1, nome_medico="Francisco", data_consulta="2020-05-02", valor_consulta=50.00)
        self.exame_d1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_d, exame="Biopsia", valor_exame=25.00)

        self.consulta_e = Consulta.objects.create(
            numero_guia=5, cod_medico=1, nome_medico="Francisco", data_consulta="2019-05-02", valor_consulta=50.00)
        self.exame_e1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_e, exame="ETM", valor_exame=25.00)

# Sinceramente, não compreendi bem o que foi pedido requisito.
    def test_consulta_average_per_mount(self):
        avg = Consulta.consulta_average(5, 2020)
        self.assertEqual(avg, 2)

    def test_exame_average_per_mount(self):
        avg = Consulta.exame_average(5, 2020)
        self.assertEqual(avg, 1.5)

    def test_consulta_total_average_per_mount(self):
        avg = Consulta.consulta_total_average(5, 2020)
        self.assertEqual(avg, 92.5)

    def test_consulta_average_per_mount_is_zero(self):
        avg = Consulta.consulta_average(4, 2020)
        self.assertEqual(avg, 0)

    def test_exame_average_per_mount_is_zero(self):
        avg = Consulta.exame_average(4, 2020)
        self.assertEqual(avg, 0)

    def test_consulta_total_average_per_mount_is_zero(self):
        avg = Consulta.consulta_total_average(4, 2020)
        self.assertEqual(avg, 0)


class DashboardExtraActionTest(APITestCase):
    def setUp(self):
        self.consulta_a = Consulta.objects.create(
            numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)
        self.exame_a1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_a, exame="RaioX", valor_exame=25.00)

        self.consulta_b = Consulta.objects.create(
            numero_guia=2, cod_medico=2, nome_medico="Chopper", data_consulta="2020-05-02", valor_consulta=40.00)
        self.exame_b1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Ultrasom", valor_exame=25.00)
        self.exame_b2 = Exame.objects.create(
            numero_guia_consulta=self.consulta_b, exame="Punção", valor_exame=45.00)

        self.consulta_c = Consulta.objects.create(
            numero_guia=3, cod_medico=3, nome_medico="Drauzio", data_consulta="2020-06-04", valor_consulta=50.00)
        self.exame_c1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_c, exame="Glicose", valor_exame=25.00)

        self.consulta_d = Consulta.objects.create(
            numero_guia=4, cod_medico=1, nome_medico="Francisco", data_consulta="2020-05-02", valor_consulta=50.00)
        self.exame_d1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_d, exame="Biopsia", valor_exame=25.00)

        self.consulta_e = Consulta.objects.create(
            numero_guia=5, cod_medico=1, nome_medico="Francisco", data_consulta="2019-05-02", valor_consulta=50.00)
        self.exame_e1 = Exame.objects.create(
            numero_guia_consulta=self.consulta_e, exame="ETM", valor_exame=25.00)

    def test_can_get_dashboard_data(self):
        url = '/api/consultas/dashboard/?competencia=2020-05'
        response = self.client.get(url)

        expect_return = {
            'consulta_media': 2,
            'exame_media': 1.5,
            'valor_consulta_media': 92.5
        }

        data = json.loads(response.content)

        self.assertEqual(data, expect_return)

    def test_cannot_get_without_param(self):
        url = '/api/consultas/dashboard/'
        response = self.client.get(url)

        except_return = "Não pode pedir por dashboard sem definir a competência"

        data = json.loads(response.content)

        self.assertEqual(data, except_return)

    def test_cannot_get_with_wrong_param_format(self):
        url = '/api/consultas/dashboard/?competencia=2020-0a'
        response = self.client.get(url)

        except_return = "A data não estava no formato apropriado"

        data = json.loads(response.content)
        self.assertEqual(data, except_return)