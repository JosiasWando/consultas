from django.db import models
from consulta.models import Consulta
# Modelo de exame vindo do banco de dados
class Exame(models.Model):
    numero_guia_consulta = models.ForeignKey(Consulta, models.DO_NOTHING, db_column='numero_guia_consulta')
    exame = models.CharField(primary_key=True, max_length=20)
    valor_exame = models.DecimalField(max_digits=1000, decimal_places=2, blank=True, null=True)

    class Meta:
        db_table = 'exame'
        unique_together = (('exame', 'numero_guia_consulta'),)