from django.test import TestCase
from .models import Exame
from consulta.models import Consulta

# Create your tests here.
class ExameModelTest(TestCase):
    def setUp(self):
        self.consulta = Consulta.objects.create(numero_guia=1, cod_medico=1, nome_medico="Francisco", data_consulta="2020-06-02", valor_consulta=50.00)

    def test_can_create_exame(self):
        exame = Exame.objects.create(numero_guia_consulta=self.consulta, exame="RaioX", valor_exame=25.00)

        exame_bd = Exame.objects.get(numero_guia_consulta=self.consulta.numero_guia)

        self.assertIsNotNone(exame_bd, "Ele não foi criado no banco")
        self.assertEqual(exame_bd.numero_guia_consulta, exame.numero_guia_consulta)
