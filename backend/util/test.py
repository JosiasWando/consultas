from unittest import TestCase
from .data import data_parans

class DateConveterTest(TestCase):
    def test_can_separate_data(self):
        data = data_parans('2020-05')
        self.assertEqual(data[0], '05')
        self.assertEqual(data[1], '2020')
    
    def test_can_cath_wrong_len(self):
        with self.assertRaises(ValueError):
            data_parans("2020-0")

    def test_can_cath_wrong_format(self):
        with self.assertRaises(ValueError):
            data_parans(2020-0)

    def test_can_cath_wrong_numbers(self):
        with self.assertRaises(ValueError):
            data_parans("2020-0a")